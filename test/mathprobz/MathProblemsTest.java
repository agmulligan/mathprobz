/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathprobz;

import mathprobz.MathProblems;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author ashleymulligan
 */
public class MathProblemsTest{

    public MathProblemsTest() {
    }

    /**
     * Test of isEven method, of class MathProblems.
     */
    @Test
    public void testIsEven() {
        System.out.println("isEven");
        int n = 6;
        boolean expResult = true;
        boolean result = MathProblems.isEven(n);
        assertEquals(expResult, result);
    }

    /**
     * Test of isOdd method, of class MathProblems.
     */
    @Test
    public void testIsOdd() {
        System.out.println("isOdd");
        int n = 3;
        boolean expResult = true;
        boolean result = MathProblems.isOdd(n);
        assertEquals(expResult, result);
    }


    /**
     * Test of parseTime method, of class MathProblems.
     */
    @Test
    public void testParseTime() {
        System.out.println("parseTime");
        int hours = 37;
        String expResult = "1 day 13 hours";
        String result = MathProblems.parseTime(hours);
        assertEquals(expResult, result);
    }
    
    
            
}
