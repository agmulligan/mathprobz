package mathprobz;

import java.io.*;
import java.lang.Math.*;

public class MathProblems {
    private static Object MILLISECONDS;



    public static boolean isEven(int n) {
        return (n % 2 == 0);
    }

    public static boolean isOdd(int n) {
        return (n % 2 != 0);
    }
    
    public static String parseTime(int hours) {
      
        int d = hours/24;
        int m = hours%24;
        return (d + " day " + m + " hours");

     
   }
    
}
